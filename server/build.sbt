name := """drone_test"""
organization := "com.pa"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.4"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.1" % Test
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.3.4"
libraryDependencies += "com.typesafe.akka" %% "akka-stream-kafka" % "0.19"

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.pa.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.pa.binders._"
