package PlayProducers

import java.util.Properties

import org.apache.kafka.clients.producer._
import models.DroneLog._

object PlayProducers {

	def sendToTopic(str: String) {
		val  props = new Properties()
		props.put("bootstrap.servers", "localhost:9092")
		
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

		val producer = new KafkaProducer[String, String](props)
		
		val TOPIC="drones"
		
			
		val record = new ProducerRecord(TOPIC, "defaultKey", str)
		producer.send(record)

		producer.close()
	}

}