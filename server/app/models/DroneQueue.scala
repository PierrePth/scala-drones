package models

import scala.collection.mutable.Queue
import java.util.concurrent.ConcurrentLinkedQueue

object DroneQueue {
    /**
      Here we use a concurrent queue to handle all the incoming request.
    */
    val queue = new ConcurrentLinkedQueue[DroneLog]()
}
