package models

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class DroneLog(id: String, latitude: String, longitude: String, temperature: String, is_busy: String, last_update: String) {
    override def toString() = { 
        "{\"id\":\"" + id + "\", \"latitude\":\"" + latitude + "\", \"longitude\" : \"" + longitude + "\", \"temperature\":\"" + temperature + "\", \"is_busy\" : \"" + is_busy + "\", \"last_update\" : \"" + last_update + "\"}"
    }
}

object DroneLog {
    implicit val reads: Reads[DroneLog] = (
        (__ \ "id").read[String] and
        (__ \ "latitude").read[String] and
        (__ \ "longitude").read[String] and
        (__ \ "temperature").read[String] and
        (__ \ "is_busy").read[String] and
        (__ \ "last_update").read[String])(DroneLog.apply _)
    }