package models

/**
 *  Drone class with :
 *    - id : the ID of the drone (unique)
 *    - latitude / longitude : coordinates of the drone
 *    - is_busy : set to 1 if the drone is busy looking for an available parking lot, 0 otherwise
 *    - last_update : The last time the drone reported to the server
 */
case class Drone(id: Int, latitude: String, longitude: String, temperature: String, is_busy: String, last_update: String) {
    override def toString() = "\n{\n    id : " + id + ", \n     Latitude : " + latitude + ", \n    Longitude : " + longitude + "temperature : " + temperature + ", \n   is_busy : " + is_busy + ", \n last update : " + last_update + "\n}"

}

/**
 *  A singleton Drone that contains an array of all the drones
 *  All the information corresponds the latest messages sent by the drones.
 *  The array is sort by id and is displayed in /DroneStatus
 */
object Drone{
    /**
     *  Create a static array of size 1000
     */
    val drones = Array.tabulate[Drone](1000)(Drone(_, "", "", "", "", ""))
    
    /**
     *  This function updates the drones contained in the array at each request
     *  the server receives.
     */ 
    def updateDrone(dronelog: DroneLog) = {
        drones(dronelog.id.toInt) = Drone(dronelog.id.toInt, dronelog.latitude, dronelog.longitude, dronelog.temperature, dronelog.is_busy, dronelog.last_update)
    }
}
