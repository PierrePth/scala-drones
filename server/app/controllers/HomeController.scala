package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import models.DroneQueue.queue
import models.Drone.drones
import play.api.libs.json._
import models.DroneLog
import scala.io.Source

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */

  /**
   *  returns the / view which is all the logs in a bootstrap table
   */
  def index() = Action {
    Ok(views.html.index(queue.toArray[DroneLog](Array[DroneLog]())))
  }

  /**
   *  returns the view with the latest informations for each drone in a
   *  bootstrap table
   */
  def droneStatus() = Action {
    Ok(views.html.droneStatus(drones))
  }

  /**
   *  returns the chart view with :
   *    - the average temperature
   *    - the number of busy vs available drones
   *    - an histogram of drones over time
   */
  def charts() = Action {
    Ok(views.html.charts(queue.toArray[DroneLog](Array[DroneLog]())))
  }

  def analytics() = Action {
    try {
      val busyInTime = Source.fromFile("app/data/busyInTime2.txt").getLines.toArray.map(x => x.slice(1, x.length()-1).split(","))
      val ko_list = Source.fromFile("app/data/disabled_drones.txt").getLines.toArray.map(x => x.slice(1, x.length()-1).split(","))

      val average_temperature = Source.fromFile("app/data/average_temperature.txt").getLines.mkString
      val time_busy = Source.fromFile("app/data/time_busy.txt").getLines.mkString
      val average_temperature_ko = Source.fromFile("app/data/average_temperature_ko.txt").getLines.mkString
      val epicenter = Source.fromFile("app/data/epicenter.txt").getLines.mkString

      val mostAvailableParkings = Source.fromFile("app/data/parking.txt").getLines.toArray.map(x => x.slice(1, x.length()-1).split(","))

      Ok(views.html.analytics(busyInTime, average_temperature, time_busy, average_temperature_ko, ko_list, epicenter, mostAvailableParkings))
    }
    catch {
      case e : Exception => 
      Ok(views.html.noData())
    }
  }

  def noData() = Action {
    Ok(views.html.noData())
  }
}
