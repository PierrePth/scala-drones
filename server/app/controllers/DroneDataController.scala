package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.libs.json._
import models.DroneLog
import models.DroneQueue.queue
import models.Drone

import play.api.data.validation.ValidationError

import PlayProducers.PlayProducers._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class DroneDataController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
   *  Create an Action to render an HTML page.
   *
   *  The configuration in the `routes` file means that this method
   *  will be called when the application receives a `GET` request with
   *  a path of `/`.
   *
   *  This function parse a request that has been sent using POST. The format
   *  of the incoming request is JSON. We create a droneLog object and add it
   *  to the log queue. This log queue will be served to the client when
   *  displaying the /index.html view
   *
   *  We also update the Drone object to take into account the last modification
   *  of the drone with the corresponding id.
   */

  def parseJson(request : Request[AnyContent]) : Result = {
    request.body.asJson.get.validate[DroneLog] match {
      case success: JsSuccess[DroneLog] => {
        val id = success.get.id
        val latitude = success.get.latitude
        val longitude = success.get.longitude
        val temperature = success.get.temperature
        val is_busy = success.get.is_busy
        val last_update = success.get.last_update
        val dronelog = DroneLog(id, latitude, longitude, temperature, is_busy, last_update)
        queue.add(dronelog)
        //queue += dronelog
        Drone.updateDrone(dronelog)
        sendToTopic(dronelog.toString())
        Ok("Log received")
      }
      case JsError(error) => BadRequest("Validation failed!")
    }
  }

  /**
   *  We do the same thing here for the CSV files
   */
  def parseCSV(content : String) : Result = {
    val attributes = content.split(",")
    queue.add(DroneLog(attributes(0), attributes(1), attributes(2), attributes(3), attributes(4), attributes(5)))
    Ok("Log received")
  }

  /**
   *  We parse the CSV file only if not empty
   */
  def validateCSV(request : Request[AnyContent]) : Result = request.body.asText match {
    case None =>  BadRequest("Empty CSV file")
    case Some(x)  =>  parseCSV(x)
  }

  /**
   *  The receive function dispatch to the corresponding parser depending on
   *  the type of the file sent.
   */
  def receive() = Action { implicit request: Request[AnyContent] => request.headers.get("Content-Type") match {
      case Some(x)                      =>  x.toString() match { 
                                                                  case "application/json" => parseJson(request)
                                                                  case "text/plain"       => validateCSV(request)
                                                                  case _                  => BadRequest("CSV here")
                                                                }
      case None                         =>  BadRequest("Wrong file type")
    }
    //Ok("Data received : " + request.body.asJson.get)
  }
}
