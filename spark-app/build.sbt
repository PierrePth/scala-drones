name := "Spark Project"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.2.0"
libraryDependencies += "org.apache.spark" % "spark-streaming_2.11" % "2.2.0"
libraryDependencies += "org.apache.spark" %% "spark-sql-kafka-0-10" % "2.2.0"
libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka-0-10" % "2.2.0"


libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6"
libraryDependencies +=  "com.typesafe.play" %% "play-json" % "2.4.0"


javacOptions ++= Seq("-source", "1.8")

resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"


