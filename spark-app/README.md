###  README  spark-app ###

This folder contains code for the consumers and the spark analytics part of the project.
We use KAFKA 2.11-1.3.0

After testing Spark Streaming and Kafka Streams, we chose to use the confluent connector to load kafka topics to HDFS.

Here is a reminder to use kafka :

#   Start Zookeeper
$   kafka_2.11-2.3.0/bin/zookeeper-server-start.sh kafka_2.11-2.3.0/config/zookeeper.properties 

#   Start Kafka Server
$   kafka_2.11-2.3.0/bin/kafka-server-start.sh kafka_2.11-2.3.0/config/server.properties

#   Start Producer
$   kafka_2.11-2.3.0/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic $TOPIC

#   Start consumer
$   kafka_2.11-2.3.0/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic $TOPIC --from-beginning

#   To create a topic
$   kafka_2.11-2.3.0/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic $TOPIC

#   To check the list of topics
$   kafka_2.11-2.3.0/bin/kafka-topics.sh --bootstrap-server localhost:9092 --list

#   To delete a topic
$   kafka_2.11-2.3.0/bin/kafka-topics.sh --delete --zookeeper localhost:2181 --topic $TOPIC

#   In order to use standalone HDFS.
    Install Hadoop 3.2.0
    follow the instructions here : https://dzone.com/articles/local-hadoop-on-laptop-for-practice
    for ssh :  
        $   sudo apt-get install openssh-server
        $   service ssh restart

    $JAVA_HOME was set doing the following :
        $   export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/

    One the installation is successfull, use
    $   bin/hdfs dfs /user/$USER
    and then check if the following work
    $   bin/hdfs dfs -ls
    The path to HDFS master :
    $   hadoop-3.2.0/bin/hdfs dfs -ls hdfs://localhost:8020