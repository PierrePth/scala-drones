package com.spark

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

import org.apache.spark.sql.types._ 
import java.util

import org.apache.kafka.clients.consumer.KafkaConsumer

import scala.collection.JavaConverters._
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions.{col, from_json}
import org.apache.spark.streaming.StreamingContext
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.dstream._
import scala.concurrent.duration._
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.streaming.Minutes
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.Seconds



object StreamProcessor {

    def consume(): Unit = {
        /**val spark =  SparkSession.builder() 
                                .appName("MyApp")
                                .getOrCreate()**/
        val conf = new SparkConf().setMaster("local[*]").setAppName("SparkStreamingProject")
        //val sparkContext = spark.sparkContext

        //import spark.implicits._
        val streamingContext = new StreamingContext(conf, Seconds(10))

        //val streamingContext = new StreamingContext(conf, Seconds(1))

        val kafkaParams = Map[String, Object](
            "bootstrap.servers" -> "localhost:9092",
            "key.deserializer" -> classOf[StringDeserializer],
            "value.deserializer" -> classOf[StringDeserializer],
            "group.id" -> "use_a_separate_group_id_for_each_stream",
            "auto.offset.reset" -> "latest",
            "enable.auto.commit" -> (false: java.lang.Boolean)
        )

        val topics = Array("drones")
        val stream = KafkaUtils.createDirectStream[String, String](
            streamingContext,
            PreferConsistent,
            Subscribe[String, String](topics, kafkaParams)
        )

           val schema = new StructType()
                    .add("id",StringType)
                    .add("latitude", StringType)
                    .add("longitude", StringType)
                    .add("temperature", StringType)
                    .add("is_busy", StringType)
                    .add("last_update", StringType)


        stream.map(record => (record.key, record.value))
        stream.foreachRDD{ rdd =>
            val sqlContext = SparkSession.builder.config(rdd.sparkContext.getConf).getOrCreate()
            import sqlContext.implicits._
            val data = rdd.map(record => record.value)
            val json = sqlContext.read.schema(schema).json(data)
            json.select("id").show
            json.write.mode(SaveMode.Append).json("../server/app/data/data.json")
            new Analytics().run()
        }

        streamingContext.start()
        streamingContext.awaitTermination()
    }
}