package com.spark

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SparkSession, DataFrame}
import org.apache.spark.sql.functions._
import java.io.{FileWriter}
import play.api.libs.json._


class Analytics {

    def run() {
        val spark =  SparkSession.builder()
                                    .appName("kafka-tutorials")
                                    .master("local[*]")
                                    .getOrCreate()


        val data = loadData()
        data.take(1).isEmpty match {
            case false => meanTemperature(data); disabledDrones(data); timeBusy(data); meanTemperatureWhenKo(data); busyInTime(data); koInTime(data); availableInTime(data); epicenter(data); mostAvailableParkings(data)
            case _ => None
        }
    }

    def loadData() : DataFrame = {
        val spark = SparkSession.builder().getOrCreate()

        val path = "../server/app/data/data.json"
        val df = spark.read.json(path)
        df.printSchema()
        df.collect.foreach(println)
        df
    }

    //mean temperature over all logs
    def meanTemperature(df: DataFrame) {
        val fw = new FileWriter("../server/app/data/average_temperature.txt", false)
        val l = df.select(mean(df("temperature"))).collect().map(_(0)).toList
        fw.write(l(0).toString)
        fw.close()
    }

    def disabledDrones(df: DataFrame) {
        val fw = new FileWriter("../server/app/data/disabled_drones.txt", false)
        df.filter(df("is_busy") === "KO").groupBy("id").agg(mean("longitude"), mean("latitude")).collect.foreach(x=>fw.write(x.toString + "\n"))
        fw.close()
    }

    //the ratio of busy time
    def timeBusy(df: DataFrame) {
        val total : Float = df.count()
        val ratio : Float = df.filter("is_busy like 'True%'").count() / total
        val fw = new FileWriter("../server/app/data/time_busy.txt", false)
        fw.write(ratio.toString)
        fw.close()
    }

    //the mean temperature on KO drones
    def meanTemperatureWhenKo(df: DataFrame) {
        val fw = new FileWriter("../server/app/data/average_temperature_ko.txt", false)
        val l = df.filter(df("is_busy") === "KO").select(mean(df("temperature"))).collect().map(_(0)).toList
        l(0) match {
            case null => fw.write("null")
            case _ => fw.write(l(0).toString)
        }
        fw.close()
    }

    def busyInTime(df: DataFrame) {
        val fw = new FileWriter("../server/app/data/busyInTime2.txt", false)
        df.filter(df("is_busy") === "True").groupBy(window(df("last_update"), "1 hour")).count().collect.foreach(x=>fw.write(x.toString + "\n"))
        //df.collect.foreach(x=>fw.write(x.toString + "\n"))
        fw.close()
    }

    def koInTime(df: DataFrame) {
        val fw = new FileWriter("../server/app/data/koInTime.txt", false)
        df.filter(df("is_busy") === "KO").groupBy(window(df("last_update"), "1 hour")).count().collect.foreach(x=>fw.write(x.toString + "\n"))
        fw.close()
    }

    def availableInTime(df: DataFrame) {
        val fw = new FileWriter("../server/app/data/availableInTime.txt", false)
        df.filter(df("is_busy") === "True").groupBy(window(df("last_update"), "1 hour")).count().collect.foreach(x=>fw.write(x.toString + "\n"))
        fw.close()
    }

    def epicenter(df: DataFrame) {
        val fw = new FileWriter("../server/app/data/epicenter.txt", false)
        df.select(mean(df("longitude")), mean(df("latitude"))).collect.foreach(x=>fw.write(x.toString + "\n"))
        fw.close()
    }

    def mostAvailableParkings(df: DataFrame) {
        val fw = new FileWriter("../server/app/data/parking.txt", false)
        df.filter(df("is_busy") === "FOUND").groupBy("longitude", "latitude").count().collect().foreach(x=>fw.write(x.toString + "\n"))
        fw.close()
    }
}