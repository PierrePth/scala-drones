import java.io._

object createData extends App {

    def create_latitudes(nb: Int) = {
        val r = scala.util.Random
        val l = List.range(1, nb)
        val random_latitude = l.map(x => r.nextGaussian() / (r.nextInt(2) + 1) + 34.052235)
        random_latitude
    }

    def create_longitudes(nb: Int) = {
        val r = scala.util.Random
        val l = List.range(1, nb)
        val random_longitude = l.map(x => r.nextGaussian() / (r.nextInt(2) + 1) - 118.243683)
        random_longitude
        }

    def create_temperatures(nb: Int) = {
        val r = scala.util.Random
        val l = List.range(1, nb)
        val random_temperature = l.map(x => r.nextGaussian() * (r.nextInt(10) + 1) + 20)
        random_temperature
    }

    def create_dates(nb: Int) = {
        val r = scala.util.Random
        val l = List.range(1, nb)
        val random_date = l.map(x => "2019-"
        + (r.nextInt(12) + 1).toString + "-"
        + (r.nextInt(30) + 1).toString + " "
        + (r.nextInt(24)).toString + "-"
        + (r.nextInt(60)).toString  + "-"
        + (r.nextInt(60)).toString)
        random_date
    }

    def create_isbusy(nb: Int) = {
        val r = scala.util.Random
        val l = List.range(1, nb)
        val random_isbusy = l.map(x => r.nextBoolean())
        random_isbusy
    }

    def write_to_file(nb: Int) = {
        val latitudes = create_latitudes(nb)
        val longitudes = create_longitudes(nb)
        val temperatures = create_temperatures(nb)
        val dates = create_dates(nb)
        val isbusy = create_isbusy(nb)
        val indexes = List.range(0, nb-1)
        val pw = new PrintWriter(new File("trip_fuzzing.txt"))
        indexes.map(i => pw.write("{\"id\": \""
        + i.toString + "\", \"latitude\": \"" 
        + latitudes(i).toString + "\", \"longitude\": \"" 
        + longitudes(i).toString + "\", \"temperature\": \"" 
        + temperatures(i).toString + "\", \"is_busy\": \"" 
        + isbusy(i) + "\", \"last_update\": \"" 
        + dates(i).toString +  "\"}\n"))
        pw.close()
    }
    
    write_to_file(11)
}

