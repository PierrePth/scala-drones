import java.io._
import java.text.SimpleDateFormat
import scala.math.BigDecimal
import scala.math

object createData extends App {
    val r = scala.util.Random
    // LA: 34.052234 Paris: 48.8534 NY: 40.7808
    def create_latitudes(nb: Int): List[Float] = {
        List.range(1, nb + 1).foldLeft((List.empty[Float], 40.7808)) {
            (acc, x) => (acc._1 :+ acc._2.toFloat, acc._2 + (r.nextFloat / 1000))
        }._1
    }
    // LA: - 118.243683 Paris: 2.348 NY: -73.9772
    def create_longitudes(nb: Int): List[Float] = {
        List.range(1, nb + 1).foldLeft((List.empty[Float], -73.9772)) {
            (acc, x) => (acc._1 :+ acc._2.toFloat, acc._2 + (r.nextFloat / 1000))
        }._1
    }

    def create_temperatures(nb: Int) : List[Float] = {
        List.range(1, nb + 1).foldLeft((List.empty[Float], 22.0)) {
            (acc, x) => (acc._1 :+ acc._2.toFloat, acc._2 + BigDecimal((r.nextGaussian() * r.nextInt(4) + 1) * 0.1).setScale(2, BigDecimal.RoundingMode.HALF_UP).toFloat)
        }._1
    }

    def create_dates(nb: Int, shift: Int) : List[String] = {
        val time = java.time.LocalDate.now.toString
        List.range(1, nb + 1).foldLeft((List.empty[String], 4500)) {
        (acc, x) => (acc._1 :+ time + "T" + (if (acc._2 / 3600 < 10) "0" else "") + acc._2 / 3600 + ":" + (if ((acc._2 % 3600) / 60 < 10) "0" else "") + (acc._2 % 3600) / 60 + ":" + (if (acc._2 % 60 < 10) "0" else "") + acc._2 % 60  + "Z", acc._2 + math.floor(r.nextGaussian() + shift).toInt)
        }._1
    }

    def create_isbusy(nb: Int) = {
        List.range(1, nb + 1).map(x => r.nextInt(r.nextInt(4) + 1) match {
        case 0 => "True"
        case 1 => "False"
        case 2 => "KO"
        case 3 => "FOUND"
        })
    }

    val m = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

    val nb = 100
    val shift = 60
    val nb_drones = 50
    val writer = new BufferedWriter(new FileWriter("./trip_fuzzing_NY.txt"))
    List.range(1, nb_drones + 1).map( m => 
            List.range(1, nb_drones).foldLeft(List.empty[String]) {
                (accu, n) => (List(create_latitudes(nb).map(x => "\", \"latitude\": \"" +  x.toString),
                                    create_longitudes(nb).map(x => "\", \"longitude\": \"" +  x.toString),
                                    create_temperatures(nb).map(x => "\", \"temperature\": \"" + x.toString),
                                    create_isbusy(nb).map(x => "\", \"is_busy\": \"" + x), 
                                    create_dates(nb, shift).map(x => "\", \"last_update\": \"" + x + "\""))
                                    .transpose
                                    .map(x => x.foldLeft("{\"id\": \"" + (m+100).toString) {
                                        (acc, y) => acc + y
                                    }).map(x => x + "}\n")
                            )
                }).flatten
                .sortBy(x => m.parse(x.slice(x.length - 23, x.length - 3)).getTime())
                .foreach(writer.write)
    writer.close()
    }
