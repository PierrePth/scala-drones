package readFiles

import java.io.File
import scala.io.Source
import scalaj.http.Http
import scalaj.http.HttpOptions
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.util.{Success, Failure}
import scalaj.http._

/**
 *  The read file object is reponsible to send data to the server using the
 *  HTTP protocole.
 */
object ReadFiles {

  /**
   *  Sends a JSON to the server (localhost here).
   *  Because we don't won't the request to be blocking, we use Futures and
   *  and handle the case of success and failure with onComplete.
   */
  def sendJson(obj: String): Unit = {
    val f: Future[HttpResponse[String]] = Future { 
        Http("http://localhost:9000/send").postData(obj)
                                          .header("Content-Type", "application/json")
                                          .header("Charset", "UTF-8")
                                          .option(HttpOptions.readTimeout(30000)).asString
    }
       
    f onComplete {
      case Success(response) => println(response.code + " : " + response.body)
      case Failure(err) => println("An error has occured : " + err.getMessage)
    }
    
    }

  /**
   *  We do exactly the same for CSV files.
   */
  def sendCSV(obj: String): Unit = {
    val f: Future[HttpResponse[String]] = Future {
        Http("http://localhost:9000/send").postData(obj)
                                          .header("Content-Type", "text/plain")
                                          .header("Charset", "UTF-8")
                                          .option(HttpOptions.readTimeout(30000)).asString
    }

    f onComplete {
      case Success(response) => println(response.code + " : " + response.body)
      case Failure(err) => println("An error has occured : " + err.getMessage)
    }
  }

  /**
   *  Walk throught a given directory and send all the files with the right
   *  extension to the server :
   *    .txt contain a list of JSON
   *    .csv files are basic CSV
   *  all the other files in the given directory won't be sent.
   */
  def readDir(dir: String): Unit = {
    new File(dir).listFiles.toIterator
                 .filter(_.isFile)
                 .filter(_.toString().endsWith(".txt"))
                 .flatMap(Source.fromFile(_).getLines)
                 .foreach(sendJson)

    new File(dir).listFiles.toIterator
                 .filter(_.isFile)
                 .filter(_.toString().endsWith(".csv"))
                 .flatMap(Source.fromFile(_).getLines)
                 .foreach(sendCSV)
  }

  /**
   *  The client must provide a directory in which are located the datafiles
   *  to send to the server (.txt and.csv files)
   */
  def main(args: Array[String]): Unit = args.length match {
    case 0 => println("args needed !")
    case _ => readDir(args(0))
  }
}
