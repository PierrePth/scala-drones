lazy val root = (project in file("."))
  .settings(
    name := "scala-drones",
    scalaVersion := "2.12.7",
    version := "0.1",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test,
    libraryDependencies += "org.scalaj" %% "scalaj-http" % "2.4.1"
  )

